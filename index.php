<?php
error_reporting(E_ALL & ~E_NOTICE);

$link = mysqli_connect("localhost", "root", "", "data");
 //$conn = new PDO("mysql:host=localhost;dbname=data", 'root', '');
if (!$link) {
  die('Not connected : ' . mysql_error());
}

$db_selected = mysqli_select_db($link, "data");
if (!$db_selected) {
  die ('Can\'t select demo : ' . mysql_error());
}

// call this file only after database connection
require_once 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
  <div id="container">
    <div id="body">
      <?php echo $emsg; ?>
      <article>
       <?php 
       $categoryList = fetchCategoryTree();
       ?>
       <div class="height20"></div>
       <h4>Recursive category tree:</h4>
       <ul>
        <?php
        $res = fetchCategoryTreeList();
        foreach ($res as $r) {
          echo  $r;
        }
        ?>
      </ul>
      <div class="height10"></div>
    </article>
    <div class="height10"></div>
  </div>
</div>
</body>
</html>
